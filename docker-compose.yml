x-lagoon-project:
  # Lagoon project name (leave `&lagoon-project` when you edit this)
  &lagoon-project gds-fm

x-volumes:
  &default-volumes
    # Define all volumes you would like to have real-time mounted into the docker containers
    volumes:
      - ./backend:/app:delegated

x-environment:
  &default-environment
    LAGOON_PROJECT: *lagoon-project

    # Route that should be used locally, if you are using pygmy, this route *must* end with .docker.amazee.io
    LAGOON_ROUTE: &default-url http://gds-fm.docker.amazee.io

    # Set the production URL
    LAGOON_PRODUCTION_URL: https://backend.gds.fm/

    # Uncomment if you like to have the system behave like in production
    #LAGOON_ENVIRONMENT_TYPE: production

    # Uncomment to enable xdebug and then restart via `docker compose up -d`
    XDEBUG_ENABLE: "true"
    # XDEBUG_MODE: "profile"

x-user:
  &default-user
    # The default user under which the containers should run. Change this if you are on linux and run with another user than id `1000`
    user: '1000'

services:

  cli: # cli container, will be used for executing composer and any local commands (drush, drupal, etc.)
    build:
      context: backend
      dockerfile: lagoon/cli.dockerfile
    image: *lagoon-project # this image will be reused as `CLI_IMAGE` in subsequent Docker builds
    labels:
      # Lagoon Labels
      lagoon.type: cli-persistent
      lagoon.persistent.name: nginx # mount the persistent storage of nginx into this container
      lagoon.persistent: /app/web/sites/default/files/ # location where the persistent storage should be mounted
    <<: [*default-volumes, *default-user]
    volumes_from: # mount the ssh-agent from the pygmy or cachalot ssh-agent
      - container:amazeeio-ssh-agent
    environment:
      << : *default-environment # loads the defined environment variables from the top
    dns: 1.1.1.1

  nginx:
    build:
      context: backend
      dockerfile: lagoon/nginx.dockerfile
      args:
        CLI_IMAGE: *lagoon-project # Inject the name of the cli image
    labels:
      lagoon.type: nginx-php-persistent
      lagoon.persistent: /app/web/sites/default/files/ # define where the persistent storage should be mounted too
    <<: [*default-volumes, *default-user]
    depends_on:
      - cli # basically just tells docker compose to build the cli first
    environment:
      << : *default-environment # loads the defined environment variables from the top
      LAGOON_LOCALDEV_URL: *default-url
    networks:
      - amazeeio-network
      - default
    dns: 1.1.1.1

  php:
    build:
      context: backend
      dockerfile: lagoon/php.dockerfile
      args:
        CLI_IMAGE: *lagoon-project
    labels:
      lagoon.type: nginx-php-persistent
      lagoon.name: nginx # we want this service be part of the nginx pod in Lagoon
      lagoon.persistent: /app/web/sites/default/files/ # define where the persistent storage should be mounted too
    <<: [*default-volumes, *default-user]
    depends_on:
      - cli # basically just tells docker compose to build the cli first
    environment:
      << : *default-environment # loads the defined environment variables from the top
    dns: 1.1.1.1

  mariadb:
    image: uselagoon/mariadb-drupal
    labels:
      lagoon.type: mariadb
    ports:
      - "3306" # exposes the port 3306 with a random local port, find it with `docker compose port mariadb 3306`
    << : *default-user # uses the defined user from top
    environment:
      << : *default-environment
    dns: 1.1.1.1

  frontend:
    command: npm run dev # this only applies for local development
    build: # how to build the frontend
      context: frontend
      dockerfile: lagoon/frontend.dockerfile
    labels:
      # Lagoon Labels
      lagoon.type: node
    volumes:
      - ./frontend/app:/app/app:delegated # specifically only mount the /frontend/app folder, so that node_module are still existing from the Docker Build
    environment:
      << : *default-environment # loads the defined environment variables from the top
      LAGOON_LOCALDEV_URL: frontend-gds-fm.docker.amazee.io
      API_SERVER: http://varnish-gds-fm.docker.amazee.io
    extra_hosts:
      - "varnish-gds-fm.docker.amazee.io:172.17.0.1" # Telling the frontend how to communicate with the drupal
      - "backend.gds.fm:172.17.0.1" # Telling the frontend how to communicate with the drupal
    networks:
      - amazeeio-network
      - default

  varnish-frontend:
    image: amazeeio/varnish
    build: # how to build the frontend
      context: frontend
      dockerfile: lagoon/varnish.dockerfile
    labels:
      lagoon.type: varnish
    links:
      - nginx # links varnish to the nginx in this docker-compose project, or it would try to connect to any nginx running in docker
    << : *default-user # uses the defined user from top
    environment:
      << : *default-environment
      VARNISH_BYPASS: "true" # by default we bypass varnish, change to 'false' or remove in order to tell varnish to cache if possible
      LAGOON_LOCALDEV_URL: gds-fm.docker.amazee.io # generate another route for varnish
    networks:
      - amazeeio-network
      - default

  varnish:
    image: amazeeio/varnish-drupal
    labels:
      lagoon.type: varnish
    links:
      - nginx # links varnish to the nginx in this docker-compose project, or it would try to connect to any nginx running in docker
    << : *default-user # uses the defined user from top
    environment:
      << : *default-environment
      VARNISH_BYPASS: "true" # by default we bypass varnish, change to 'false' or remove in order to tell varnish to cache if possible
      LAGOON_LOCALDEV_URL: varnish-gds-fm.docker.amazee.io # generate another route for varnish
    networks:
      - amazeeio-network
      - default
networks:
  amazeeio-network:
    external: true
