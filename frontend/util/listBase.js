import jsonApi from '@/api/jsonApiClient'

const state = () => ({
  allItems: {
    items: [],
    page: 0
  },
  filteredItems: {
    items: [],
    page: 0
  },
  filters: {
    search: '',
    tag: '_all'
  },
  filtersSet: false,
  limit: 48,
  lastPromise: null
})

const mutations = {
  addAllItems(state, payload) {
    const newItems = [...state.allItems.items, ...payload]
    state.allItems.items = Object.freeze(newItems)
  },
  incrementAllItemsPage(state) {
    state.allItems.page++
  },
  addFilteredItems(state, payload) {
    if (typeof payload !== 'undefined') {
      const newItems = [...state.filteredItems.items, ...payload]
      state.filteredItems.items = Object.freeze(newItems)
    }
  },
  incrementFilteredItemsPage(state) {
    state.filteredItems.page++
  },
  resetFilteredItems(state) {
    state.filteredItems.items = []
  },
  resetFilteredPage(state) {
    state.filteredItems.page = 0
  },
  setFilters(state, payload) {
    state.filters = payload
    if (
      payload.search.length > 0 ||
      (typeof payload.tag === 'string' && payload.tag !== '_all')
    ) {
      state.filtersSet = true
    } else {
      state.filtersSet = false
    }
  },
  setLastPromise(state, value) {
    state.lastPromise = value
  }
}

const actions = {
  async fetchItemsBase(context, payload = {}) {
    const query = payload.query
    if (context.state.filtersSet === true) {
      query.page = context.getters.getPageQuery
      context.commit('incrementFilteredItemsPage')
      if (payload.intial === true) {
        const result = await jsonApi.get(payload.endpoint, this.$axios, query)
        return result
      } else {
        const items = await jsonApi.get(payload.endpoint, this.$axios, query)
        context.commit('addFilteredItems', items)
      }
    } else {
      query.page = context.getters.getPageQuery
      context.commit('incrementAllItemsPage')
      const items = await jsonApi.get(payload.endpoint, this.$axios, query)
      context.commit('addAllItems', items)
    }
  },
  async setFilters(context, payload) {
    context.commit('resetFilteredPage')
    await context.commit('setFilters', payload)
    const promise = context.dispatch('fetchItems', { intial: true })
    context.commit('setLastPromise', promise)
    promise.then((result) => {
      if (promise === context.state.lastPromise) {
        context.commit('resetFilteredItems')
        context.commit('setLastPromise', null)
        context.commit('addFilteredItems', result)
      }
    })
    return promise
  },
  async resetFilters(context) {
    context.commit('resetFilteredPage')
    await context.commit('setFilters', {
      search: '',
      tag: '_all'
    })
  }
}

const getters = {
  items(state) {
    if (state.filtersSet === true) {
      return state.filteredItems.items
    } else {
      return state.allItems.items
    }
  },
  itemsExcludingIds: (state, getters) => (ids) => {
    return getters.items.filter((item) => !ids.includes(item.id))
  },
  filters(state) {
    return state.filters
  },
  getPageQuery(state) {
    let page
    if (state.filtersSet === true) {
      page = state.filteredItems.page
    } else {
      page = state.allItems.page
    }
    return {
      limit: state.limit,
      offset: page * state.limit
    }
  }
}

export default { state, mutations, actions, getters }
