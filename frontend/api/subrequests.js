import jsonApiParse from 'jsonapi-parse'
import buildQueryString from '@/api/jsonapiQueryString'

export default class SubRequests {
  constructor(endpoint = '/subrequests?_format=json') {
    this.autoincrementedId = 0
    this.endpoint = endpoint
    this.requests = []
  }

  add(request) {
    // only add this request if this requestId is not already in the pipe
    const existing = this.requests.find(
      (item) => item.requestId === request.requestId
    )
    if (existing) return
    const defaultOptions = {
      action: 'view',
      headers: {
        Accept: 'application/json'
      }
    }
    const subrequest = Object.assign(defaultOptions, request)
    if (subrequest.options) {
      subrequest.uri =
        subrequest.uri + '?' + buildQueryString(subrequest.options)
    }
    // generate automatically a requestId if needed
    if (subrequest.requestId === undefined) {
      subrequest.requestId = ++this.autoincrementedId
    }
    this.requests.push(subrequest)
  }

  textToBinary(string) {
    return string
      .split('')
      .map(function(char) {
        return char.charCodeAt(0).toString(2)
      })
      .join(' ')
  }

  /**
   * @param {string} response : raw *body* of the http response sended by subrequests drupal module
   */
  parseResponse(responseBody) {
    const responses = {}
    for (const key in responseBody.data) {
      const json = JSON.parse(responseBody.data[key].body)
      responses[key] = jsonApiParse.parse(json)
    }
    return responses
  }

  getUrl() {
    const blueprint = JSON.stringify(this.requests)
    const url = this.endpoint + '&query=' + encodeURIComponent(blueprint)
    return url
  }
}
