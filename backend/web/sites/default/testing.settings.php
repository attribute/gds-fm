<?php

include __DIR__ . '/staging.settings.php';

// Don't show any error messages on the site (will still be shown in watchdog).
$config['system.logging']['error_level'] = 'verbose';
