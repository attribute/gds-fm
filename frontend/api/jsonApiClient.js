/**
 * Client for JSON API server.
 * We use "jsonapi-parse" package to format responses :
 * It resolves relationships and included objects nicely in the final data object.
 */
import qs from 'qs'

export default {
  get: (uri, axios, params = null) => {
    const query = params ? '?' + qs.stringify(params, { indices: false }) : ''
    const url = `/api/${uri}${query}`
    return axios.get(url)
  }
}
