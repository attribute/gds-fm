<?php
/**
 * @file
 * amazee.io Drupal 8 development environment configuration file.
 *
 * This file will only be included on development environments.
 *
 * It contains some defaults that the amazee.io team suggests, please edit them as required.
 */

// Show all error messages on the site.
$config['system.logging']['error_level'] = 'verbose';

// Expiration of cached pages on Varnish to 15 min.
$config['system.performance']['cache']['page']['max_age'] = 900;

// Disable Google Analytics from sending dev GA data.
$config['google_analytics.settings']['account'] = 'UA-XXXXXXXX-YY';

// Log mails and don't send them
$config['symfony_mailer.mailer_policy._']['configuration']['symfony_mailer_log'] = [];
$config['symfony_mailer.mailer_policy._']['configuration']['email_transport']['value'] = 'null';

// Set Matomo Site ID to 11, the testing website
$config['matomo.settings']['site_id'] = 11;

// Stage file proxy URL from production URL.
if (getenv('LAGOON_PRODUCTION_URL')){
  $config['stage_file_proxy.settings']['origin'] = getenv('LAGOON_PRODUCTION_URL');
  $config['stage_file_proxy.settings']['origin_dir'] = 'sites/default/files';
  $config['stage_file_proxy.settings']['hotlink'] = FALSE;
  // This should be set to TRUE once this issue is fixed:
  // https://www.drupal.org/project/stage_file_proxy/issues/3332930
  // This prevents from working on image styles locally without syncing
  // the original images.
  $config['stage_file_proxy.settings']['use_imagecache_root'] = FALSE;
  $config['stage_file_proxy.settings']['verify'] = FALSE;
}

// Get project specific settings
if (file_exists(__DIR__ . '/custom/development.settings.php')) {
  include __DIR__ . '/custom/development.settings.php';
}
// Get project specific services
if (file_exists(__DIR__ . '/custom/development.yml')) {
  $settings['container_yamls'][] = __DIR__ . '/custom/development.services.yml';
}
