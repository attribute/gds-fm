export default function parseDomain(uri) {
  const domain = uri
    .replace('http://', '')
    .replace('https://', '')
    .replace('//', '')
    .replace('www.', '')
    .split(/[/?#]/)[0]
  const dotCount = (domain.match(/\./g) || []).length
  return domain.split(/[.]/)[dotCount - 1]
}
