<?php

namespace Drupal\gds_migrate\Plugin\migrate\process;

use Drupal\Component\Utility\NestedArray;
use Drupal\migrate\Row;
use Drupal\migrate_plus\Plugin\migrate\process\EntityGenerate;


/**
 * This plugin generates entities within the process plugin.
 *
 * @MigrateProcessPlugin(
 *   id = "entity_generate_nested"
 * )
 *
 * @see EntityLookup
 *
 * All the configuration from the lookup plugin applies here. In its most
 * simple form, this plugin needs no configuration. If there are fields on the
 * generated entity that are required or need some value, their values can be
 * provided via values and/or default_values configuration options.
 *
 * Example usage with values and default_values configuration:
 * @code
 * field_images:
 *   -
 *     plugin: get
 *     source: field_gallery
 *   -
 *     plugin: multiple_values
 *   -
 *     plugin: entity_generate_nested
 *     entity_type: media
 *     bundle: image
 *     value_key: field_media_image
 *     bundle_key: vid
 *     default_values:
 *       bundle: image
 *     values:
 *       field_media_image/0/target_id: fid
 *       field_media_image/0/alt: alt
 * @endcode
 */
class EntityGenerateNested extends EntityGenerate {

  /**
   * Fabricate an entity.
   *
   * This is intended to be extended by implementing classes to provide for more
   * dynamic default values, rather than just static ones.
   *
   * @param mixed $default_value
   *   Primary value to use in creation of the entity.
   *
   * @return array
   *   Entity value array.
   */
  protected function entity($value) {
    $entity_values = [];

    // Gather any static default values for properties/fields.
    if (isset($this->configuration['default_values']) && is_array($this->configuration['default_values'])) {
      foreach ($this->configuration['default_values'] as $key => $default_value) {
        $entity_values[$key] = $default_value;
      }
    }
    // Gather any additional properties/fields.
    if (isset($this->configuration['values']) && is_array($this->configuration['values'])) {
      foreach ($this->configuration['values'] as $key => $property) {
        // TODO: Remove this logic once 8.6 is no longer supported.
        // See https://www.drupal.org/project/migrate_plus/issues/3043199
        if (version_compare(\Drupal::VERSION, '8.7', '>=')) {
          $source_value = $this->row->get($property);
        }
        else {
          $getProcessPlugin = $this->processPluginManager->createInstance('get', ['source' => $property]);
          $source_value = $getProcessPlugin->transform(NULL, $this->migrateExecutable, $this->row, $property);
        }
        if ($source_value === NULL && isset($value[$property])) {
          $source_value = $value[$property];
        }
        NestedArray::setValue($entity_values, explode(Row::PROPERTY_SEPARATOR, $key), $source_value, TRUE);
      }
    }

    return $entity_values;
  }

}
