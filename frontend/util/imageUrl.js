export default function buildImageUrl(imageUri, style, apiUrl) {
  const imageBaseUrl = apiUrl + '/sites/default/files/styles'
  const imageFilePath = imageUri.replace('public://', '')
  return `${imageBaseUrl}/${style}/public/${encodeURIComponent(imageFilePath)}`
}
