# GDS.FM

This repository contains the backend and frontend for [gds.fm](https://gds.fm/). It is built using [Nuxt.js](https://nuxtjs.org), [Vue.js](https://vuejs.org/) and [Drupal](https://www.drupal.org/).

## Frontend installation

### Requirements

* [node](https://nodejs.org/) (tested with 12.18.3)

### Installating & running

``` bash
# Go to frontend folder
$ cd frontend

# Install dependencies
$ npm ci

# Generate static project
$ npm run build

# Start webserver
$ npm run start
```

### Development

``` bash
# Serve with hot reload at localhost:3000
$ npm run dev
```

### Setting a different API endpoint

You can copy `.env.example` to `.env` and edit the API endpoint. Restarting the build process is needed to apply this change.

## Backend

### Requirements

* [Docker](https://docs.docker.com/engine/install/)
* [Docker Compose](https://docs.docker.com/compose/install/)
* [pgymy](https://lagoon.readthedocs.io/en/latest/using_lagoon/local_development_environments/)

### Installation

``` bash
# Start pygmy
$ pygmy up

# Start docker container
$ docker-compose up -d

# ssh into the docker container
$ docker-compose exec cli bash

# Install dependencies
$ composer install

# Install Drupal
$ drush si --existing-config minimal

# Generate login link
$ drush uli
```

Now you should have a running install of the backend at [http://gds-fm.docker.amazee.io/](http://gds-fm.docker.amazee.io/)

## Credits
* [ATTRIBUTE](https://www.attribute.ch/) (Development)
* [Michel Luarasi](https://michelluarasi.com/) (Design)
* [Joris Noordermeer](https://noordermeer.ch/) (Initial development of the audio player)
