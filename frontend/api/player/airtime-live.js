import parseDate from '@/util/parseDate'

export async function getCurrentTrack(axios, bustCache) {
  const now = new Date()
  const axiosOptions = {}
  if (bustCache) {
    axiosOptions.params = {
      ts: Math.ceil(now.getTime() / 15000)
    }
  }
  const response = await axios.get(
    'https://gdsfm.airtime.pro/api/live-info-v2?timezone=utc',
    axiosOptions
  )
  const data = response.data

  const show = data.shows.current

  const currentTrack = parseTrack(data.tracks.current, show)
  const nextTrack = parseTrack(
    data.tracks.next,
    show,
    currentTrack.remainingTime
  )

  const program = [show, data.shows.next[0]]

  return {
    tracks: {
      current: currentTrack,
      next: nextTrack
    },
    program
  }
}

function parseTrack(track, show, remainingTimeOffset = 0) {
  let title
  let artist = ''

  if (track.type !== 'livestream' && track.metadata) {
    if (track.metadata.track_title !== '') {
      artist = track.metadata.artist_name
      title = track.metadata.track_title
    } else {
      const curTrack = track.name.split(' - ')
      artist = curTrack[0]
      title = curTrack[1]
    }

    if (artist === 'GDS.FM') {
      title = artist
      artist = ''
    }
  } else {
    title = show.name
    artist = show.description || ''
  }

  const id = generateTrackId(track, show)
  const cover = show.image_path !== '' ? show.image_path : null
  const url = show.url !== '' ? show.url : null
  const remainingTime = parseRemainingTime(track.ends, remainingTimeOffset)

  return {
    id,
    artist,
    title,
    cover,
    url,
    remainingTime
  }
}

function parseRemainingTime(time, remainingTimeOffset = 0) {
  const now = new Date()
  const ends = parseDate(time)
  return Math.ceil(ends - now.getTime()) - remainingTimeOffset
}

function generateTrackId(trackData, showData) {
  if (!trackData) return showData.instance_id || showData.id
  if (trackData.metadata) return trackData.metadata.id || trackData.metadata.md5
  else
    return trackData.starts
      .toString()
      .replace(/\s+/g, '-')
      .replace(/[^\w-]+/g, '')
}
