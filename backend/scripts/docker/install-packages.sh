#!/usr/bin/env bash

set -ve

if [ "$ATTRIBUTE_PHP_PACKAGES" != "false" ] && [ "$ATTRIBUTE_PHP_PACKAGES" != "" ] ; then
  eval "apk add ${ATTRIBUTE_PHP_PACKAGES}"
fi
