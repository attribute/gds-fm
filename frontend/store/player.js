import debounce from 'lodash/debounce'
import { getCurrentTrack } from '@/api/player/airtime-live'
import { getCover } from '@/api/player/lastfm'

let fetchTracksTimeout = null

// root state object.
export const state = () => ({
  firstTrackPlayed: false,
  isPlaying: false,
  isLoading: false,

  track: {
    id: null,
    title: null,
    cover: null,
    artist: null,
    url: null
  },
  program: []
})

// mutations
export const mutations = {
  /*
    Player State
     */
  playing(state, value) {
    state.isPlaying = value
  },

  loading(state, value) {
    state.isLoading = value
  },

  firstVinyl(state) {
    state.firstVinyl = true
  },

  /*
    Track Metadata
     */
  track(state, data) {
    if (state.track.id !== null) {
      state.firstTrackPlayed = true
      // console.log('first track played!')
    }
    state.track = data
  },
  cover(state, url) {
    state.track.cover = url
  },
  program(state, data) {
    // console.log('Commit programm')
    // console.log(data)
    state.program = data
  }
}

// actions
export const actions = {
  fetchTrackInfoDebounced({ dispatch, commit, getters }) {
    clearTimeout(fetchTracksTimeout)

    return getCurrentTrack(this.$axios, getters.firstTrackHasLoaded)
      .then((data) => {
        // has the track changed?
        // console.log(data)
        const currentTrack = data.tracks.current
        const nextTrack = data.tracks.next
        if (getters.currentTrack.id !== currentTrack.id) {
          // store new track info
          commit('track', currentTrack)

          if (currentTrack.cover == null) {
            // no cover present:
            // load and attach to latest track..
            dispatch('loadCover', currentTrack)
          }
        } else {
          // // console.log('Track has not changed!')
        }
        commit('program', data.program)

        // console.log('fetchTracksTimeout: ' + currentTrack.remainingTime)

        // set timeout to next api call
        fetchTracksTimeout = setTimeout(() => {
          dispatch('fetchTrackInfo')
        }, currentTrack.remainingTime + 15000)

        // Set next track
        setTimeout(() => {
          // console.log('Set next track!')
          commit('track', nextTrack)
          // @TODO we should be already fetching the next cover before changing the track. But this is not possible with the current store data structure.
          dispatch('loadCover', nextTrack)
        }, currentTrack.remainingTime)
      })
      .catch((e) => {
        // console.log('Error!')
        // console.log(e)

        fetchTracksTimeout = setTimeout(() => {
          dispatch('fetchTrackInfo')
        }, 2000)
        // console.log('Failed. fetchTracksTimeout: ' + 2000)
      })
  },

  fetchTrackInfo: debounce(function(store, router, storeModule, values) {
    // console.log('fetchTrackInfo!')
    store.dispatch('fetchTrackInfoDebounced', values)
  }, 1000),

  loadCover({ commit }, track) {
    return getCover(track).then((cover) => {
      commit('cover', cover)
    })
  },

  setPlaying(context, payload) {
    context.commit('playing', payload)
    if (payload === true) {
      context.commit('loading', true)
    } else {
      context.commit('loading', false)
    }
  }
}

// getters
export const getters = {
  currentTrack: (state) => state.track,
  program: (state) => state.program,
  firstTrackHasLoaded: (state) => state.track.id !== null,
  firstTrackPlayed: (state) => state.firstTrackPlayed,
  isPlaying: (state) => state.isPlaying,
  isLoading: (state) => state.isLoading
}
