import SubRequests from '@/api/subrequests'

export default async function entityByRoute(route, options, axios) {
  const subrequests = new SubRequests()
  subrequests.add({
    requestId: 'router',
    uri: '/router/translate-path',
    options: {
      path: route
    }
  })
  subrequests.add({
    requestId: 'entity',
    uri: '{{router.body@$.jsonapi.individual}}',
    options,
    waitFor: ['router'],
    headers: {
      'Content-Type': 'application/vdn.api+json'
    }
  })
  const response = await axios.get(subrequests.getUrl())
  const responses = subrequests.parseResponse(response)
  return responses
}
