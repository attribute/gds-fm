import cloneDeep from 'lodash/cloneDeep'
import listBase from '@/util/listBase'

const defaultQuery = {
  filter: {
    status: {
      value: 1
    }
  },
  include: ['field_image', 'field_image.field_media_image'],
  fields: {
    'node--news_post': ['title,path,field_image,field_date'],
    // @TODO: why is the created field needed?!
    'media--image': ['created,field_media_image'],
    'file--file': ['uri,meta']
  },
  sort: {
    field_date: {
      path: 'field_date',
      direction: 'DESC'
    }
  }
}

const endpoint = 'node/news_post'

export const state = listBase.state
export const mutations = listBase.mutations
export const actions = {
  ...listBase.actions,
  async fetchItems(context, payload = {}) {
    payload.endpoint = endpoint
    const query = cloneDeep(defaultQuery)
    if (context.state.filtersSet === true) {
      payload.endpoint = 'index/news'
      query.filter.fulltext = context.state.filters.search
      // Below are implementations of the search_api index. Currently on OR operation is supported...
      /* query.filter.andgroup = {
        group: {
          conjunction: 'AND'
        }
      }
      query.filter.fulltext = {
        condition: {
          path: 'fulltext',
          value: context.state.filters.search,
          memberOf: 'andgroup'
        }
      }
      if (context.state.filters.tag !== '_all') {
        query.filter.field_artist_category = {
          condition: {
            path: 'field_artist_category',
            value: context.state.filters.tag,
            memberOf: 'andgroup'
          }
        }
      } */
    }
    payload.query = query
    const result = await context.dispatch('fetchItemsBase', payload)
    return result
  }
}
export const getters = listBase.getters
