import Vue from 'vue'
import VueMatomo from 'vue-matomo'

export default ({ app }) => {
  Vue.use(VueMatomo, {
    router: app.router,
    host: 'https://prism.attribute.ch',
    siteId: 5,
    disableCookies: true
  })
}
