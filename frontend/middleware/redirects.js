export default function(req, res, next) {
  let redirect = null
  if (req.headers.host === 'www.gds.fm') {
    redirect = `https://gds.fm${req.url}`
  }
  if (redirect !== null) {
    res.writeHead(301, { Location: redirect })
    res.end()
    return
  }
  next()
}
