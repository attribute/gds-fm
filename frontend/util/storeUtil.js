import debounce from 'lodash/debounce'

export default {
  async fetch($store, storeModule) {
    const countItems = $store.getters[`${storeModule}/items`].length
    if (countItems === 0) {
      const result = await $store.dispatch(`${storeModule}/fetchItems`)
      return result
    }
    return null
  },
  async infiniteHandler($store, $state, storeModule) {
    const oldCountItems = $store.getters[`${storeModule}/items`].length
    await $store.dispatch(`${storeModule}/fetchItems`)
    const newCountItems = $store.getters[`${storeModule}/items`].length
    if (oldCountItems === newCountItems) {
      $state.complete()
    } else {
      $state.loaded()
    }
  },
  handleFilterInput: debounce(function(store, router, storeModule, values) {
    if (values.search.length !== '' || values.tag !== '_all') {
      router.push({ query: values })
    } else {
      router.push({ query: {} })
    }
    const self = this
    self.filterInput(store, storeModule, values)
  }, 200),
  async filterInput(store, storeModule, values) {
    await store.dispatch(`${storeModule}/setFilters`, values)
  }
}
