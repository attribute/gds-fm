require('dotenv').config()
const aspectRatio = require('v-aspect-ratio/dist/v-aspect-ratio.ssr')

const apiUrl = process.env.API_SERVER || 'https://backend.gds.fm'
const appUrl = process.env.APP_URL || 'https://gds.fm'

module.exports = {
  vue: {
    config: {
      performance: true
    }
  },
  server: {
    // port: 8000, // default: 3000
    host: '0.0.0.0' // default: localhost
  },
  serverMiddleware: ['@/server/httpHeaders.js', '~/middleware/redirects.js'],

  publicRuntimeConfig: {
    axios: {
      baseURL: apiUrl
    },
    apiUrl,
    appUrl
  },
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: '%s - GDS.FM',
    title: 'GDS.FM',
    htmlAttrs: {
      lang: 'de'
    },
    meta: [
      { charset: 'utf-8' },
      {
        hid: 'viewport',
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        name: 'MobileOptimized',
        content: 'width'
      },
      {
        name: 'HandheldFriendly',
        content: 'true'
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      },
      {
        name: 'twitter:card',
        content: 'summary_large_image'
      }
    ],
    link: [
      {
        rel: 'preconnect',
        href: 'https://use.typekit.net',
        crossorigin: true
      },
      {
        rel: 'preconnect',
        href: 'https://gdsfm.airtime.pro',
        crossorigin: true
      },
      {
        rel: 'preconnect',

        /* ,
        {
          rel: 'preconnect',
          href: 'https://itunes.apple.com/',
          crossorigin: true
        } */
        href: 'https://p.typekit.net',
        crossorigin: true
      },
      {
        rel: 'preconnect',
        href: 'https://ws.audioscrobbler.com',
        crossorigin: true
      },
      {
        rel: 'preconnect',
        href: 'https://lastfm.freetls.fastly.net',
        crossorigin: true
      },
      {
        rel: 'preconnect',
        href: apiUrl,
        crossorigin: true
      },

      {
        rel: 'stylesheet',
        href: 'https://use.typekit.net/fne7dvm.css'
      }
    ]
  },
  pwa: {
    meta: {
      ogHost: appUrl,
      ogUrl: false,
      ogTitle: false,
      ogImage: {
        path: '/icon.png',
        width: false,
        height: false,
        type: false
      }
    }
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#666' },

  /*
   ** Page transitions
   */
  pageTransition: 'default',

  /*
   ** Global CSS
   */
  css: ['@/assets/sass/styles.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '@/plugins/axios.js' },
    { src: '@/plugins/vue-infinite-loading.js', ssr: false },
    { src: '@/plugins/vue-lazysizes.client.js', ssr: false },
    { src: '@/plugins/vue-matomo.js', ssr: false },
    { src: '@/plugins/svg4everybody.js', ssr: false },
    { src: '@/plugins/vue-router-referer.js', ssr: false },
    { src: '~/plugins/vue-badger-accordion.js', ssr: false },
    { src: '~/plugins/global-components.js', ssr: false }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ['@nuxtjs/style-resources'],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/dotenv',
    '@nuxtjs/proxy',
    '@nuxtjs/svg-sprite',
    'vue-scrollto/nuxt',
    'nuxt-responsive-loader',
    '@nuxtjs/redirect-module',
    'cookie-universal-nuxt'
  ],

  proxy: {
    '/sitemap.xml': apiUrl,
    '/sitemap.xsl': apiUrl,
    '/core/assets/vendor/jquery/jquery.js': apiUrl,
    '/modules/contrib/xmlsitemap/xsl/jquery.tablesorter.min.js': apiUrl,
    '/modules/contrib/xmlsitemap/xsl/xmlsitemap.xsl.js': apiUrl
  },

  /*
   ** Sass setup
   */
  styleResources: {
    scss: ['@/assets/sass/tools.scss']
  },

  /*
   ** SVG sprite setup
   */
  svgSprite: {
    input: '~/assets/images'
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }
    },
    postcss: {
      plugins: {
        'postcss-plugin-px2rem': {
          rootValue: 16
        }
      },
      preset: {
        autoprefixer: {
          grid: true
        }
      }
    },

    transpile: ['last-fm', 'isstream', 'vue-router-referer', 'simple-get'],

    // Fixing infinite reload on HMR
    devMiddleware: {
      headers: {
        'Cache-Control': 'no-store',
        Vary: '*'
      }
    }
  },
  render: {
    bundleRenderer: {
      directives: {
        'aspect-ratio': aspectRatio.default
      }
    }
  },
  redirect: [
    { from: '^/programm', to: '/sender', statusCode: 301 },
    { from: '^/gdsfm-member-werden', to: '/member', statusCode: 301 },
    { from: '^/ueber-uns', to: '/about', statusCode: 301 },
    { from: '^/we-play', to: '/about', statusCode: 301 },
    { from: '^/kontaktformular', to: '/kontakt', statusCode: 301 },
    {
      from: '^/shop',
      to: 'https://gdsfm.payrexx.com/de/pay?tid=4ce8fc50',
      statusCode: 301
    },
    { from: '^/renew', to: '/member', statusCode: 301 },
    { from: '^/pay', to: '/member', statusCode: 301 },
    { from: '^/sender', to: '/events', statusCode: 301 }
  ],
  responsiveLoader: {
    max: 2560,
    steps: 8,
    placeholder: true
  }
}
