import parseDate from '@/util/parseDate'

export async function fetchProgram(axios) {
  const response = await axios.get(
    'https://gdsfm.airtime.pro/api/week-info?timezone=utc'
  )
  const data = response.data
  const weekdays = [
    'monday',
    'tuesday',
    'wednesday',
    'thursday',
    'friday',
    'saturday',
    'sunday',
    'nextmonday',
    'nexttuesday',
    'nextwednesday',
    'nextthursday',
    'nextfriday',
    'nextsaturday',
    'nextsunday'
  ]
  const sortedWeekData = []
  const now = new Date()
  let currentDay = 0

  for (const day in weekdays) {
    // get data of the day and push to array
    const dayData = data[weekdays[day]]

    // continue if no data
    if (dayData.length > 0) {
      const firstShowOfTheDayStartTime = parseDate(dayData[0].starts)

      if (now > firstShowOfTheDayStartTime) {
        // first show of the day has started
        currentDay = Object.keys(weekdays).indexOf(day)
      }

      sortedWeekData.push({
        date: firstShowOfTheDayStartTime,
        shows: dayData
      })
    }
  }

  return {
    days: sortedWeekData,
    currentDay
  }
}
