export async function searchApi(term, axios) {
  // term = 'gorillaz'

  const response = await axios.get('https://itunes.apple.com/search', {
    params: {
      term,
      limit: 1,
      media: 'music',
      country: 'CH',
      entity: 'musicTrack'
    }
  })
  const data = response.data
  return data.resultCount ? data.results[0].trackViewUrl + '&at=11lxKC' : false
}
