ARG CLI_IMAGE
FROM ${CLI_IMAGE} as cli

FROM uselagoon/php-8.3-fpm
COPY scripts /app/scripts

# RUN /app/scripts/docker/install-packages.sh

COPY --from=cli /app /app
