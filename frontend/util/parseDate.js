export default function parseDate(date) {
  const isoDate = date.replace(' ', 'T')
  return new Date(isoDate + '.000+00:00')
}
