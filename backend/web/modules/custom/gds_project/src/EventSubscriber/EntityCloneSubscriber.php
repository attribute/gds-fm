<?php

namespace Drupal\gds_project\EventSubscriber;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_clone\Event\EntityCloneEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Sends an email when the order transitions to Fulfillment.
 */
class EntityCloneSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'entity_clone.pre_clone' => 'handlePreClone',
      'entity_clone.post_clone' => 'handlePostClone',
    ];
    return $events;
  }

  /**
   * Modifies entity values before cloning
   *
   * @param \Drupal\entity_clone\Event\EntityCloneEvent $event
   *   The  event.
   */
  public function handlePreClone(EntityCloneEvent $event) {
    /** @var EntityInterface $cloned_entity */
    $cloned_entity = $event->getClonedEntity();
    $entity_type_id = $cloned_entity->getEntityTypeId();
    if ($entity_type_id === 'node') {
      /** @var \Drupal\node\Entity\Node $node */
      $node = $cloned_entity;
      $node->set('status', 0);
    }
  }

  /**
   * Redirects after cloning
   *
   * @param \Drupal\entity_clone\Event\EntityCloneEvent $event
   *   The  event.
   */
  public function handlePostClone(EntityCloneEvent $event) {
    \Drupal::request()->query->remove('destination');
    /** @var EntityInterface $cloned_entity */
    $cloned_entity = $event->getClonedEntity();
    $url = $cloned_entity->toUrl('edit-form', ['absolute' => TRUE]);
    $string = $url->toString();
    $response = new RedirectResponse($string);
    $response->send();
  }

}
