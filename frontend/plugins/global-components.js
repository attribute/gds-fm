import Vue from 'vue'

import ParagraphsContent from '@/components/global/ParagraphsContent'

Vue.component('ParagraphsContent', ParagraphsContent)
