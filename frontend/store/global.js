export const state = () => ({
  navigation: {
    state: false,
    items: {
      primary: [
        {
          title: 'Radio',
          to: '/'
        },
        {
          title: 'Events',
          to: '/events'
        },
        {
          title: 'Artists',
          to: '/artists'
        }
      ],
      secondary: [
        {
          title: 'About Us',
          to: '/about'
        },
        {
          title: 'Support us',
          to: '/member'
        },
        {
          title: 'News',
          to: '/news'
        },
        {
          title: 'Shop',
          to: 'https://gdsfm.payrexx.com/de/pay?tid=4ce8fc50'
        },
        {
          title: 'Kontakt',
          to: '/kontakt'
        }
      ],
      meta: [
        {
          title: 'Legal',
          to: '/legal'
        },
        {
          title: 'Impressum',
          to: '/impressum'
        }
      ]
    }
  },
  socialLinks: [
    {
      title: 'Facebook',
      uri: 'https://www.facebook.com/gds.fm'
    },
    {
      title: 'Instagram',
      uri: 'https://instagram.com/gdsfm'
    },
    {
      title: 'Hearthis',
      uri: 'https://hearthis.at/gdsfm'
    },
    {
      title: 'Spotify',
      uri:
        'https://open.spotify.com/user/k8kenujrio1r2n3kmpfqo9ujz?si=hh9KEZICRnqxd70luMlUqA'
    },
    {
      title: 'Twitter',
      uri: 'https://twitter.com/GDSFM'
    },
    {
      title: 'Vimeo',
      uri: 'https://vimeo.com/gdsfm'
    },
    {
      title: 'Telegram',
      uri: 'https://t.me/s/gdsfmsender'
    },
    {
      title: 'Youtube',
      uri: 'https://www.youtube.com/channel/UCEaYeuasi0N6gowuepSegtQ'
    }
  ],
  monthColors: {
    1: '#d6d461',
    2: '#df83a9',
    3: '#7da9db',
    4: '#b2b2b2',
    5: '#ef7875',
    6: '#eed26a',
    7: '#9bc5ac',
    8: '#b4907a',
    9: '#9c91c5',
    10: '#ee754d',
    11: '#67a664',
    12: '#778aac'
  }
})

export const mutations = {
  setNavigationState(state, payload) {
    state.navigation.state = payload
  }
}

export const actions = {
  async setNavigationState(context, payload) {
    await context.commit('setNavigationState', payload)
  }
}

export const getters = {
  navigation(state) {
    return state.navigation
  },
  socialLinks(state) {
    return state.socialLinks
  }
}
