import cloneDeep from 'lodash/cloneDeep'
import listBase from '@/util/listBase'
import formatDate from '@/util/formatDate'

const today = new Date()
// console.log('today: ', today)
if (today.getHours() < 5) {
  today.setDate(today.getDate() - 1)
}
today.setHours(0, 0, 0, 0)
// console.log('today corrected: ', today)

const defaultQuery = {
  filter: {
    status: {
      value: 1
    },
    field_date_range: {
      path: 'field_date_range.value',
      operator: '>',
      value: ''
    }
  },
  include: ['field_image', 'field_image.field_media_image'],
  fields: {
    'node--event': ['title,path,field_image,field_date_range'],
    // @TODO: why is the created field needed?!
    'media--image': ['created,field_media_image'],
    'file--file': ['uri,meta']
  },
  sort: {
    title: {
      path: 'field_date_range.value',
      direction: 'ASC'
    }
  }
}

const endpoint = 'node/event'

export const state = listBase.state
export const mutations = listBase.mutations
export const actions = {
  ...listBase.actions,
  async fetchItems(context, payload = {}) {
    payload.endpoint = endpoint
    const query = cloneDeep(defaultQuery)

    const today = new Date()
    // console.log('today2: ', today)
    if (today.getHours() < 4) {
      // console.log('today2: corrected!')
      today.setDate(today.getDate() - 1)
    }
    today.setHours(6, 0, 0, 0)
    // console.log('today2 corrected: ', today)

    query.filter.field_date_range.value = today.toISOString()

    payload.query = query
    const result = await context.dispatch('fetchItemsBase', payload)
    // console.log(result)
    return result
  }
}
export const getters = {
  ...listBase.getters,
  itemsByDate(state) {
    const items = new Map()
    for (const key in state.allItems.items) {
      const item = state.allItems.items[key]
      const date = new Date(item.field_date_range.value)
      const year = date.getFullYear()
      const month = date.getMonth()
      const yearMonth = `${year}${month}`
      let currentMonth
      if (items.has(yearMonth)) {
        currentMonth = items.get(yearMonth)
      } else {
        currentMonth = {
          title: formatDate(date, 'month_year'),
          monthNumber: formatDate(date, 'month_number'),
          items: new Map()
        }
        items.set(yearMonth, currentMonth)
      }
      currentMonth.items.set(key, item)
    }

    return items
  }
}
