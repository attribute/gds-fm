export default function stripHTML(html) {
  return html
    .replace(/(<([^>]+)>)/gi, '')
    .replace(/(\r\n|\n|\r)/gm, '')
    .trim()
}
