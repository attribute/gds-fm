import cloneDeep from 'lodash/cloneDeep'
import listBase from '@/util/listBase'

const defaultQuery = {
  filter: {
    status: {
      value: 1
    }
  },
  include: ['field_image', 'field_image.field_media_image'],
  fields: {
    'node--artist': ['title,path,field_image'],
    // @TODO: why is the created field needed?!
    'media--image': ['created,field_media_image'],
    'file--file': ['uri,meta']
  },
  sort: {
    changed: {
      path: 'changed',
      direction: 'DESC'
    }
  }
}

const endpoint = 'node/artist'

export const state = listBase.state
export const mutations = listBase.mutations
export const actions = {
  ...listBase.actions,
  async fetchItems(context, payload = {}) {
    payload.endpoint = endpoint
    const query = cloneDeep(defaultQuery)
    if (context.state.filtersSet === true) {
      query.filter.title = {
        condition: {
          path: 'title',
          operator: 'CONTAINS',
          value: context.state.filters.search
        }
      }
      if (context.state.filters.tag !== '_all') {
        query.filter.tags = {
          condition: {
            path: 'field_artist_category.id',
            value: context.state.filters.tag
          }
        }
      }
    }
    payload.query = query
    const result = await context.dispatch('fetchItemsBase', payload)
    return result
  }
}
export const getters = listBase.getters
