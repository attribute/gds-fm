import { fetchProgram } from '@/api/player/airtime-week'

// root state object.
export const state = () => ({
  days: [],
  today: 0,
  currentDay: 0
})

// mutations
export const mutations = {
  /*
    Programm
     */
  program(state, data) {
    state.days = data.days
    state.today = data.today
    state.currentDay = data.currentDay
  }
}

// actions
export const actions = {
  fetchProgram({ commit }) {
    return fetchProgram(this.$axios).then((program) => {
      commit('program', program)
    })
  }
}

// getters
export const getters = {}
