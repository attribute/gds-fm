export default function formatDate(date, format = 'short') {
  if (typeof date === 'string') {
    date = new Date(date)
  }

  let options = {}

  switch (format) {
    case 'date':
      options = {
        day: '2-digit',
        month: '2-digit'
      }
      break
    case 'short':
      options = {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric'
      }
      break
    case 'long':
      options = {
        day: '2-digit',
        month: 'long',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit'
      }
      break
    case 'date_day':
      options = {
        day: 'numeric'
      }
      break
    case 'weekday_short':
      options = {
        weekday: 'short'
      }
      break
    case 'weekday':
      options = {
        weekday: 'long'
      }
      break
    case 'time':
      options = {
        hour: '2-digit',
        minute: '2-digit'
      }
      break
    case 'month_number':
      options = {
        month: 'numeric'
      }
      break
    case 'month_year':
      options = {
        month: 'long',
        year: 'numeric'
      }
      break
    default:
      break
  }

  const timeFormats = ['time']
  if (timeFormats.includes(format)) {
    return date.toLocaleTimeString('de-DE', options)
  }

  return date.toLocaleDateString('de-DE', options)
}
