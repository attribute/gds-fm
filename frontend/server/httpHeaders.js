export default (req, res, next) => {
  res.setHeader('Cache-Control', 's-maxage=999, stale-while-revalidate')
  next()
}
