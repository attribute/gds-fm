const LastFM = require('last-fm')

export function getCover(track) {
  const lastfmapi = new LastFM('32ed7a4705f895f6e8d7529cd1efadb4', {
    userAgent: 'GdsPlayer/1.0.0 (https://play.gds.fm)'
  })

  return new Promise((resolve, reject) => {
    lastfmapi.trackSearch(
      {
        q: track.artist + ' ' + track.title,
        limit: 1
      },
      (err, data) => {
        if (err || data.result.length < 1) {
          return false
        }

        const result = data.result[0]

        lastfmapi.trackInfo(
          {
            name: result.name,
            artistName: result.artistName,
            limit: 1
          },
          (err, data) => {
            if (err || data.images === undefined || data.images.length < 1) {
              return false
            }

            resolve(data.images[data.images.length - 1])
          }
        )
      }
    )
  })
}
