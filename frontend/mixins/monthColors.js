import formatDate from '@/util/formatDate'

export default {
  methods: {
    monthNumber(date = new Date()) {
      return formatDate(date, 'month_number')
    },
    monthColor(monthNumber) {
      return this.$store.state.global.monthColors[monthNumber]
    },
    monthBackgroundGradient(monthNumber) {
      const monthColor = this.monthColor(monthNumber)
      return {
        background: `linear-gradient(90deg, ${monthColor} 0%, ${monthColor} 80px, transparent 80px, transparent 100%`
      }
    },
    monthBackgroundColor(monthNumber) {
      const monthColor = this.monthColor(monthNumber)
      return {
        background: monthColor
      }
    }
  }
}
