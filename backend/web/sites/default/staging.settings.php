<?php
/**
 * @file
 * amazee.io Drupal 8 production environment configuration file.
 *
 * This file will only be included on production environments.
 *
 * It contains some defaults that the amazee.io team suggests, please edit them as required.
 */

// Don't show any error messages on the site (will still be shown in watchdog).
$config['system.logging']['error_level'] = 'hide';

// Expiration of cached pages on Varnish to 15 min.
$config['system.performance']['cache']['page']['max_age'] = 900;

// Aggregate CSS files on.
$config['system.performance']['css']['preprocess'] = 1;

// Aggregate JavaScript files on.
$config['system.performance']['js']['preprocess'] = 1;

// Log mails and send them using sendrid
$config['symfony_mailer.mailer_policy._']['configuration']['symfony_mailer_log'] = [];
$config['symfony_mailer.mailer_policy._']['configuration']['email_transport']['value'] = 'sendgrid';

// Set Matomo Site ID to 11, the testing website
$config['matomo.settings']['site_id'] = 11;

// Stage file proxy URL from production URL.
if (getenv('LAGOON_PRODUCTION_URL')){
  $config['stage_file_proxy.settings']['origin'] = getenv('LAGOON_PRODUCTION_URL');
  $config['stage_file_proxy.settings']['origin_dir'] = 'sites/default/files';
  $config['stage_file_proxy.settings']['hotlink'] = FALSE;
  // This should be set to TRUE once this issue is fixed:
  // https://www.drupal.org/project/stage_file_proxy/issues/3332930
  // This prevents from working on image styles locally without syncing
  // the original images.
  $config['stage_file_proxy.settings']['use_imagecache_root'] = FALSE;
  $config['stage_file_proxy.settings']['verify'] = FALSE;
}

// Get project specific settings
if (file_exists(__DIR__ . '/custom/staging.settings.php')) {
  include __DIR__ . '/custom/staging.settings.php';
}
// Get project specific services
if (file_exists(__DIR__ . '/custom/staging.yml')) {
  $settings['container_yamls'][] = __DIR__ . '/custom/staging.services.yml';
}
