FROM amazeeio/node:12-builder as builder
COPY package.json package-lock.json /app/
RUN npm ci

FROM amazeeio/node:12
COPY --from=builder /app/node_modules /app/node_modules
COPY . /app/

ENV NODE_ENV production

RUN npm run build
RUN fix-permissions /home/.config/

EXPOSE 3000
CMD ["npm", "start"]
