import formatDate from '@/util/formatDate'

export default {
  computed: {
    formattedDateLong() {
      return formatDate(this.date.value, 'long')
    },
    formattedDateDay() {
      return formatDate(this.date.value, 'date_day')
    },
    formattedDateWeekday() {
      return formatDate(this.date.value, 'weekday_short')
    },
    formattedDate() {
      if (this.date === null) {
        return 'EMPTY DATE!'
      }
      return formatDate(this.date)
    },
    formattedDateRange() {
      return (
        formatDate(this.date.value, 'long') +
        ' - ' +
        formatDate(this.date.end_value, 'time')
      )
    }
  }
}
